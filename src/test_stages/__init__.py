# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Run `tox` on several groups of environments, stopping on errors."""

VERSION = "0.2.0"
