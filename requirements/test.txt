# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

contextlib-chdir >= 1, < 2; python_version < '3.11'
pytest >= 6, < 9
